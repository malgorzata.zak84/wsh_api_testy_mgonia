import requests
import random
import endpoints


def test_calc_add():
    first_number, second_number = random.randint(0, 1000), random.randint(0, 1000)
    expected_result = first_number + second_number

    body = {
        'firstNumber': first_number,
        'secondNumber': second_number
    }

    add_numbers_response = requests.post(endpoints.calculator_add, json=body)
    assert add_numbers_response.status_code == 200
    print(add_numbers_response.json())

    add_result_list = add_numbers_response.json()
    actual_result = add_result_list['result']
    assert actual_result == expected_result



