import requests
import endpoints



def test_get_ping():
    ping_response = requests.get(endpoints.ping)
    assert ping_response.status_code == 200
    assert ping_response.text == 'pong'
    print(ping_response.text)


def test_ping_as_json():
    headers_dict = {
        "Accept": "application/json"
    }
    ping_response = requests.get(endpoints.ping, headers=headers_dict)
    assert ping_response.status_code == 200
    response_dict = ping_response.json()
    assert response_dict['reply'] == 'pong!'
